package citing

import (
	"crypto/sha512"
	"encoding/base64"
	"encoding/hex"
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"git.openprivacy.ca/sarah/kdb/fetch"
	"github.com/nickng/bibtex"
	"io/ioutil"
	"os"
	"os/exec"
	"path"
	"strings"
)

func ExtractPDF(extract string) (string, string, string, *bibtex.BibEntry) {
	log.Infof("Extracting..." + extract)
	parts := strings.Split(extract, ";")
	if len(parts) >= 3 {
		args := strings.Split(parts[1], ":")

		src := strings.Join(parts[2:], ":")
		id := sha512.Sum512([]byte(src))
		fetch.Fetch(src, hex.EncodeToString(id[:]))
		cachepath := path.Join("cache/", hex.EncodeToString(id[:]))
		args = append(args, "-singlefile", "-png", cachepath, "tmp")
		subProcess := exec.Command("pdftoppm", args...)
		log.Infof("Running.. %v", subProcess.Args, subProcess.String())
		err := subProcess.Run()
		output, _ := subProcess.CombinedOutput()
		log.Infof("Success: %v %v", output, err)
		data, _ := ioutil.ReadFile("tmp.png")
		link := hex.EncodeToString([]byte(extract))
		log.Infof("Extracted; %v", link)
		bt := FindReference(src)
		title := "From " + src
		if bt != nil {
			title = bt.Fields["title"].String() + " by " + bt.Fields["author"].String() + " (" + bt.Fields["year"].String() + ")"
		}
		return title, link, "<img class=\"source\" id=\"" + link + "\" src=\"data:image/png;base64, " + base64.StdEncoding.EncodeToString(data) + "\"/>", bt
	}

	return "", "", "", nil
}

func FindReference(url string) *bibtex.BibEntry {
	file, err := os.Open("./data/references.kdb")
	if err == nil {
		bt, err := bibtex.Parse(file)
		if err != nil {
			log.Errorf("%v", err)
		}
		for _, e := range bt.Entries {
			if e.Fields["url"].String() == url {
				return e
			}
		}
	}
	return nil
}
