package fetch

import (
	"git.openprivacy.ca/openprivacy/libricochet-go/log"
	"golang.org/x/net/proxy"
	"io/ioutil"
	"net/http"
	"os"
	"path"
)

func makeTorifiedClient() *http.Client {
	torDialer, err := proxy.SOCKS5("tcp", "127.0.0.1:9050", nil, proxy.Direct)
	if err != nil {
		//log.Fatalf("Could not connect to Tor Proxy: %v", err)
	}
	transportConfig := &http.Transport{
		Dial: torDialer.Dial,
	}
	client := new(http.Client)
	client.Transport = transportConfig
	client.CheckRedirect = func(r *http.Request, via []*http.Request) error {
		r.URL.Opaque = r.URL.Path
		return nil
	}
	return client
}

func Fetch(url string, cacheid string) []byte {
	log.Infof("Fetching..." + url)
	cachepath := path.Join("cache/", cacheid)
	if _, err := os.Stat(cachepath); !os.IsNotExist(err) {
		data, _ := ioutil.ReadFile(cachepath)
		return data
	}

	client := makeTorifiedClient()
	resp, err := client.Get(url)

	if err == nil {
		defer resp.Body.Close()
		data, _ := ioutil.ReadAll(resp.Body)
		ioutil.WriteFile(cachepath, data, 0640)
		return data
	}
	log.Errorf("Error fetching: %v", err)
	return []byte{}
}
